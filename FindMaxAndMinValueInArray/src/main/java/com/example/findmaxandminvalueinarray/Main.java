
package com.example.findmaxandminvalueinarray;

public class Main {
    public static void main(String[] args) {
        int [] arr ={4,8,3,9,6,2};
        int foundMinValue = findMinimumValue(arr);
        int foundMaxValue = findMaximumValue(arr);
        
        System.out.println("Minimum value : " + foundMinValue);
        System.out.println("Maximum value : " + foundMaxValue);
    }
    
    public static int findMinimumValue(int [] arr){
        int min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if(arr[i] < min){
                min = arr[i];
            }
        }
        return min;
    }
    
    public static int findMaximumValue(int[] arr){
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if(arr[i] > max){
                max = arr[i];
            }
        }
        
        return max;
    }
}
